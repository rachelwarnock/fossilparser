# Parse fossil range graph input file
#
# Parse fossil range graph input file for dppdiv from fossil occurrence data.
#
# @param tree Phylo object (this isn't used).
# @param fossils Fossil dataframe (may contain correlation bounds or not).
# @param seqs Phylip sequence string.
# @param dates Specify how to treat fossil ages (fixed, median, range).
# @param file File name.
beast.xml<-function(tree,fossils,seqs,dates="fixed",file=""){
  tree<-tree # this isn't used
  fossils<-fossils
  seqs<-seqs
  dates<-dates # "fixed", "median", "range"
  file<-file

  cat("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><beast namespace=\"beast.core:beast.evolution.alignment:beast.evolution.tree.coalescent:beast.core.util:beast.evolution.nuc:beast.evolution.operators:beast.evolution.sitemodel:beast.evolution.substitutionmodel:beast.evolution.likelihood:beast.evolution.speciation:beast.core.parameter:beast.util\" version=\"2.0\">\n\n\n",file=file,append=F)

  # parse sequence data
  cat("\t<data dataType=\"nucleotide\" id=\"alignment\" name=\"alignment\">\n\n",file=file,append=T)
  parse.seqs(seqs,file=file)
  n=unlist(strsplit(seqs[1]," "))[3]
  parse.empty.seqs(fossils,n=n,file=file)
  cat("\n\t</data>\n\n",file=file,append=T)

  # tip/traits dates
  cat("\t<trait id=\"tipDates\" spec=\"beast.evolution.tree.TraitSet\" traitname=\"date-backward\" units=\"year\" value=\"\n",file=file,append=T)
  if(dates=="fixed"){
    for(i in 1:length(fossils$h)){
      cat("f",i,"=",fossils$h[i],",\n",sep="",file=file,append=T)
    }
  }
  else if(dates=="median"){
    for(i in 1:length(fossils$h)){
      cat("f",i,"=",median(c(fossils$min[i],fossils$max[i])),",\n",sep="",file=file,append=T)
    }
  }
  # tips
  tips=as.numeric(unlist(strsplit(seqs[1]," "))[2])
  for(i in 2:tips){
    tip=unlist(strsplit(seqs[i]," "))[1]
    cat(tip,"=0.0,\n",sep="",file=file,append=T)
  }
  tip=unlist(strsplit(seqs[tips+1]," "))[1]
  cat(tip,"=0.0\n",file=file,append=T)
  cat("\t\">\n\t\t<taxa id=\"taxonSet\" spec=\"beast.evolution.alignment.TaxonSet\" alignment=\"@alignment\"/>\n\t</trait>\n\n",file=file,append=T)

  # ? specify the starting tree
  cat("\t<tree spec=\"beast.util.ClusterZBSATree\" id=\"tree\" nodetype=\"beast.evolution.tree.ZeroBranchSANode\" clusterType=\"upgma\" trait=\"@tipDates\">\n",file=file,append=T)
  cat("\t\t<taxa idref=\"alignment\"/>\n",file=file,append=T)
  cat("\t</tree>\n\n",file=file,append=T)

  # specify the SA FBD model
  # detail and think about paras
  cat("\t<BirthDeathSkylineModel spec=\"SABDSamplingThroughTimeModel\" id=\"birthDeath\" tree=\"@tree\" conditionOnRhoSampling=\"true\">\n",file=file,append=T)
  cat("\t\t<parameter name=\"origin\" id=\"origin\" value =\"100.\" lower=\"0.\"/>\n",file=file,append=T)
  cat("\t\t<parameter name=\"diversificationRate\" id=\"diversificationRate\" value=\"0.5\"/>\n",file=file,append=T)
  cat("\t\t<parameter name=\"turnover\" id=\"turnover\" value=\".5\"/>\n",file=file,append=T)
  cat("\t\t<parameter name=\"samplingProportion\" id=\"samplingProportion\" value=\"0.7\"/>\n",file=file,append=T)
  cat("\t\t<parameter name=\"removalProbability\" id=\"r\" value=\"0.0\" lower=\"0.\" upper=\"1.\"/>\n",file=file,append=T)
  cat("\t\t<parameter name=\"rho\" id=\"rho\" value=\"0.7\" lower=\"0.\" upper=\"1.\"/>\n",file=file,append=T)
  cat("\t</BirthDeathSkylineModel>\n\n",file=file,append=T)

  # site model
  cat("\t<siteModel gammaCategoryCount=\"1\" id=\"SiteModel\" spec=\"SiteModel\">\n",file=file,append=T)
  cat("\t\t<parameter dimension=\"1\"  id=\"mutationRate\" name=\"mutationRate\" value=\"1.0\"/>\n",file=file,append=T)
  cat("\t\t<substModel id=\"gtr\" rateAC=\"@rateAC\" rateGT=\"@rateGT\" rateAT=\"@rateAT\" rateCG=\"@rateCG\" rateCT=\"@rateCT\" spec=\"GTR\">\n",file=file,append=T)
  cat("\t\t<parameter dimension=\"1\"  id=\"rateAG\" lower=\"0.0\" name=\"rateAG\" value=\"1.0\"/>\n",file=file,append=T)
  cat("\t\t<frequencies estimate=\"true\" id=\"freqs\" spec=\"Frequencies\">\n",file=file,append=T)
  cat("\t\t<parameter name='frequencies' id='freqParameter' value='0.25' dimension='4' lower=\"0.\" upper=\"1.\"/>\n",file=file,append=T)
  cat("\t\t</frequencies>\n",file=file,append=T)
  cat("\t\t</substModel>\n",file=file,append=T)
  cat("\t</siteModel>\n\n",file=file,append=T)

  # clock model
  cat("\t<branchRateModel id=\"StrictClock\" spec=\"beast.evolution.branchratemodel.StrictClockModel\">\n",file=file,append=T)
	cat("\t\t<parameter dimension=\"1\" id=\"clock.rate\" name=\"clock.rate\" value=\"0.05\" lower=\"0.\"/>\n",file=file,append=T)
  cat("\t</branchRateModel>\n\n",file=file,append=T)

  cat("\t<!-- likelihood and priors: -->\n",file=file,append=T)
  cat("\t<distribution id=\"posterior\" spec=\"util.CompoundDistribution\">\n",file=file,append=T)
  cat("\t\t<distribution id=\"prior\" spec=\"util.CompoundDistribution\">\n",file=file,append=T)
  cat("\t\t\t<distribution  x=\"@clock.rate\" id=\"clockRatePrior\" spec=\"beast.math.distributions.Prior\">\n",file=file,append=T)
  cat("\t\t\t\t<distr spec='beast.math.distributions.LogNormalDistributionModel' id='clockRatePrior.exp' M=\"-4.6\" S=\"1.25\"/>\n",file=file,append=T)
  cat("\t\t\t</distribution>\n",file=file,append=T)
  cat("\t\t\t<distribution id=\"hky.frequencies.prior\" spec=\"beast.math.distributions.Prior\" x=\"@freqParameter\">\n",file=file,append=T)
  cat("\t\t\t\t<distr id=\"Uniform \" lower=\"0.0\" upper=\"1.0\" spec=\"beast.math.distributions.Uniform\"/>\n",file=file,append=T)
  cat("\t\t\t</distribution>\n",file=file,append=T)
  cat("\t\t\t<distribution id=\"diversificationRate_prior\" spec=\"beast.math.distributions.Prior\" x=\"@diversificationRate\">\n",file=file,append=T)
  cat("\t\t\t\t<distr spec=\"beast.math.distributions.Uniform\" lower=\"0.\" upper=\"1000.\" offset=\"0.\"/>\n",file=file,append=T)
  cat("\t\t\t</distribution>\n",file=file,append=T)
  cat("\t\t\t<distribution id=\"turnover_prior\" spec=\"beast.math.distributions.Prior\" x=\"@turnover\">\n",file=file,append=T)
  cat("\t\t\t\t<distr spec=\"beast.math.distributions.Uniform\" lower=\"0.\" upper=\"1.\" offset=\"0.\"/>\n",file=file,append=T)
  cat("\t\t\t</distribution\n>",file=file,append=T)
  cat("\t\t\t<distribution id=\"samplingProportion_prior\" spec=\"beast.math.distributions.Prior\" x=\"@samplingProportion\">\n",file=file,append=T)
  cat("\t\t\t\t<distr spec=\"beast.math.distributions.Uniform\" lower=\"0\" upper=\"1.\" offset=\"0.\"/>\n",file=file,append=T)
  cat("\t\t\t</distribution>\n",file=file,append=T)
  cat("\t\t\t<distribution  id='origin_Prior' x=\"@origin\" spec=\"beast.math.distributions.Prior\">\n",file=file,append=T)
  cat("\t\t\t\t<distr spec='beast.math.distributions.Uniform' lower=\"0.\" upper=\"1000.\" offset=\"0.\"/>\n",file=file,append=T)
  cat("\t\t\t</distribution>\n",file=file,append=T)
  cat("\t\t\t<distribution id=\"BDlikelihood\" spec=\"util.CompoundDistribution\">\n",file=file,append=T)
  cat("\t\t\t\t<distribution idref=\"birthDeath\"/>\n",file=file,append=T)
  cat("\t\t\t</distribution>\n",file=file,append=T)
  cat("\t\t</distribution>\n",file=file,append=T)
  cat("\t\t<distribution id=\"jointTreeLikelihood\" spec=\"util.CompoundDistribution\">\n",file=file,append=T)
  cat("\t\t\t<distribution data=\"@alignment\" id=\"treeLikelihood\" spec=\"TreeLikelihood\" tree=\"@tree\" siteModel=\"@SiteModel\" branchRateModel=\"@StrictClock\"/>\n",file=file,append=T)
  cat("\t\t</distribution>\n",file=file,append=T)
  cat("\t</distribution>\n\n",file=file,append=T)

  # set up chain options
  cat("\t<run chainLength=\"200000000\" id=\"mcmc\" spec=\"MCMC\" storeEvery=\"100000\" distribution=\"@posterior\">\n\n",file=file,append=T)

  cat("\t\t<state id=\"state\" storeEvery=\"100000\">\n",file=file,append=T)
  cat("\t\t\t<stateNode idref=\"tree\"/>\n",file=file,append=T)
  cat("\t\t\t<stateNode idref=\"origin\"/>\n",file=file,append=T)
  cat("\t\t\t<stateNode idref=\"diversificationRate\"/>\n",file=file,append=T)
  cat("\t\t\t<stateNode idref=\"turnover\"/>\n",file=file,append=T)
  cat("\t\t\t<stateNode idref=\"samplingProportion\"/>\n",file=file,append=T)
  #cat("\t\t<!-- \t<stateNode idref=\"r\"/> -->\n",file=file,append=T)

  cat("\t\t\t<stateNode idref=\"clock.rate\"/>\n",file=file,append=T)
  cat("\t\t\t<stateNode idref=\"freqParameter\"/>\n",file=file,append=T)
  cat("\t\t\t<parameter dimension=\"1\"  id=\"rateAC\" lower=\"0.0\" upper=\"100.0\" name=\"stateNode\" value=\"1.0\"/>\n",file=file,append=T)
  cat("\t\t\t<parameter dimension=\"1\"  id=\"rateGT\" lower=\"0.0\" upper=\"100.0\" name=\"stateNode\" value=\"1.0\"/>\n",file=file,append=T)
  cat("\t\t\t<parameter dimension=\"1\"  id=\"rateAT\" lower=\"0.0\" upper=\"100.0\" name=\"stateNode\" value=\"1.0\"/>\n",file=file,append=T)
  cat("\t\t\t<parameter dimension=\"1\"  id=\"rateCG\" lower=\"0.0\" upper=\"100.0\" name=\"stateNode\" value=\"1.0\"/>\n",file=file,append=T)
  cat("\t\t\t<parameter dimension=\"1\"  id=\"rateCT\" lower=\"0.0\" upper=\"100.0\" name=\"stateNode\" value=\"1.0\"/>\n",file=file,append=T)
  cat("\t\t</state>\n\n",file=file,append=T)

  # operators FBD model
  cat("\t\t<operator id=\"origOperator\" spec=\"ScaleOperator\" scaleFactor=\".75\" weight=\"3\" parameter=\"@origin\"/>\n",file=file,append=T)
  cat("\t\t<operator id=\"diversificationRateOperator\" spec=\"ScaleOperator\" scaleFactor=\".75\" weight=\"10\" parameter=\"@diversificationRate\"/>\n",file=file,append=T)
  cat("\t\t<operator id=\"turnoverOperator\" spec=\"ScaleOperator\" scaleFactor=\".75\" weight=\"10\" parameter=\"@turnover\"/>\n",file=file,append=T)
  cat("\t\t<operator id=\"samplingProportionOperator\" spec=\"ScaleOperator\" scaleFactor=\".75\" weight=\"10\" parameter=\"@samplingProportion\"/>\n",file=file,append=T)
  cat("\t\t<!-- <operator id=\"rOperator\" spec=\"ScaleOperator\" scaleFactor=\".9\" weight=\"1\" parameter=\"@r\"/> -->\n\n",file=file,append=T)
  # tree operators
  cat("\t\t<operator spec=\"TreeDimensionJump\" weight=\"10\" tree=\"@tree\"/>\n",file=file,append=T)
  cat("\t\t<operator spec=\"WilsonBaldingForZeroBranchSampledAncestorTrees\" weight=\"10\" tree=\"@tree\"/>\n",file=file,append=T)
  cat("\t\t<operator spec=\"ExchangeForZeroBranchSampledAncestorTrees\" isNarrow=\"false\" weight=\"10\" tree=\"@tree\"/>\n",file=file,append=T)
  cat("\t\t<operator spec=\"ExchangeForZeroBranchSampledAncestorTrees\" weight=\"10\" tree=\"@tree\"/>\n",file=file,append=T)
  cat("\t\t<operator spec=\"UniformForZeroBranchSATrees\" weight=\"20\" tree=\"@tree\"/>\n",file=file,append=T)
  cat("\t\t<operator id=\"rootScaler\" spec=\"ScaleOperatorForZeroBranchSATrees\" scaleFactor=\".95\" weight=\"1\" tree=\"@tree\" rootOnly=\"true\"/>\n",file=file,append=T)
  cat("\t\t<operator id=\"treeScaler\" spec=\"ScaleOperatorForZeroBranchSATrees\" scaleFactor=\".95\" weight=\"10\" tree=\"@tree\"/>\n\n",file=file,append=T)
  # clock model operators
  cat("\t\t<operator id=\"clock.rateScaler\" parameter=\"@clock.rate\" scaleFactor=\"0.75\" spec=\"ScaleOperator\" weight=\"10\"/>\n\n",file=file,append=T)
  # substitution model operators
  cat("\t\t<operator spec=\"ScaleOperator\" scaleFactor=\".75\" weight=\".2\" parameter=\"@rateAC\"/>\n",file=file,append=T)
  cat("\t\t<operator spec=\"ScaleOperator\" scaleFactor=\".75\" weight=\".2\" parameter=\"@rateAT\"/>\n",file=file,append=T)
  cat("\t\t<operator spec=\"ScaleOperator\" scaleFactor=\".75\" weight=\".2\" parameter=\"@rateCG\"/>\n",file=file,append=T)
  cat("\t\t<operator spec=\"ScaleOperator\" scaleFactor=\".75\" weight=\".2\" parameter=\"@rateCT\"/>\n",file=file,append=T)
  cat("\t\t<operator spec=\"ScaleOperator\" scaleFactor=\".75\" weight=\".2\" parameter=\"@rateGT\"/>\n",file=file,append=T)
  cat("\t\t<operator autoOptimize=\"true\" delta=\"0.2\" id=\"FrequenciesExchanger\" integer=\"false\" spec=\"DeltaExchangeOperator\" weight=\"0.1\" parameter=\"@freqParameter\"/>\n\n",file=file,append=T)

  # logging parameters
  cat("\t\t<logger fileName=\"$(seed).log\" id=\"tracelog\" logEvery=\"10000\" mode=\"autodetect\" model=\"@posterior\">\n",file=file,append=T)
  cat("\t\t\t<distribution idref=\"posterior\" name=\"log\"/>\n",file=file,append=T)
  cat("\t\t\t<log idref=\"BDlikelihood\"/>\n",file=file,append=T)
  cat("\t\t\t<log idref=\"jointTreeLikelihood\"/>\n",file=file,append=T)
  cat("\t\t\t<log id=\"treeHeight\" spec=\"beast.evolution.tree.TreeHeightLogger\" tree=\"@tree\"/>\n",file=file,append=T)
  cat("\t\t\t<log idref=\"origin\"/>\n",file=file,append=T)
  cat("\t\t\t<log idref=\"diversificationRate\"/>\n",file=file,append=T)
  cat("\t\t\t<log idref=\"turnover\"/>\n",file=file,append=T)
  cat("\t\t\t<log idref=\"samplingProportion\"/>\n",file=file,append=T)
  cat("\t\t\t<!-- <log idref=\"r\"/> -->\n",file=file,append=T)
  cat("\t\t\t<log id=\"SACount\" spec=\"beast.evolution.tree.SALogger\" tree=\"@tree\"/>\n",file=file,append=T)
  cat("\t\t\t<log idref=\"clock.rate\"/>\n",file=file,append=T)
  cat("\t\t\t<log idref=\"freqParameter\"/>\n",file=file,append=T)
  cat("\t\t\t<log idref=\"rateAC\"/>\n",file=file,append=T)
  cat("\t\t\t<log idref=\"rateAG\"/>\n",file=file,append=T)
  cat("\t\t\t<log idref=\"rateAT\"/>\n",file=file,append=T)
  cat("\t\t\t<log idref=\"rateCG\"/>\n",file=file,append=T)
  cat("\t\t\t<log idref=\"rateCT\"/>\n",file=file,append=T)
  cat("\t\t\t<log idref=\"rateGT\"/>\n",file=file,append=T)
  cat("\t\t</logger>\n\n",file=file,append=T)
  # tree logger
  cat("\t\t<!-- <logger fileName=\"$(seed).trees\" id=\"treelog\" logEvery=\"200000\" mode=\"tree\">\n\t\t<tree idref=\"tree\" name=\"log\"/>\n\t\t</logger> -->\n\n",file=file,append=T)

  cat("\t\t</run>\n\n",file=file,append=T)
  cat("</beast>\n\n",file=file,append=T)

 #EOF
}

# parse simualted sequences
parse.seqs<-function(seqs,file=""){
  seqs<-seqs
  file<-file

  for(i in 2:length(seqs)){
    data=unlist(strsplit(seqs[i]," +"))
    cat("\t\t<sequence taxon=\"",data[1],"\" value=\"",data[2],"\"/>\n",sep="",file=file,append=T)
  }
  #eof
}

# parse empty alignments for fossil taxa
parse.empty.seqs<-function(fossils,n,file=""){
  fossils<-fossils
  n<-n #seq length
  file=file

  id=1

  for(i in fossils$h){
    cat("\t\t<sequence taxon=\"f",id,"\" value=\"",rep("-",n),"\"/>\n",sep="",file=file,append=T)
    id=id+1
  }
  #eof
}

