# Function to calculate node ages of a non-ultrametric tree using the TreeSim function getx
n.ages<-function(tree){

  depth = ape::node.depth.edgelength(tree)
  node.ages = max(depth) - depth
  names(node.ages) <- 1:(tree$Nnode+length(tree$tip))

  # adding possible offset if tree fully extinct
  if(!is.null(tree$root.time)) node.ages = node.ages + tree$root.time - max(node.ages)

  return(node.ages)
}

# Identify parent nodes
ancestor<-function(edge,tree){
  edge<-edge
  tree<-tree

  parent<-tree$edge[,1][which(tree$edge[,2]==edge)]

  return(parent)
  #eof
}

# Identify tips
#
# @param taxa Edge label.
# @param tree Phylo object.
# @return Boolean (true/false).
# @examples
# t<-ape::rtree(6)
# is.tip(t$edge[,2][6],t)
is.tip<-function(taxa,phylo){

  tree<-phylo

  if (length(which(tree$edge[,1]==taxa)) < 1) {
    return(1)
  }
  else {
    return(0)
  }

  # EOF
}

# Identify the root
root<-function(tree){
  tree<-tree

  root=length(tree$tip.label)+1

  return(root)
  #eof
}

# Test is root
is.root<-function(edge,tree){
  edge<-edge
  tree<-tree

  root=length(tree$tip.label)+1

  if(edge == root)
    return(TRUE)
  else
    return(FALSE)
  #eof
}

# fetch immediate decendants
descendants<-function(edge,tree){
  edge<-edge
  tree<-tree

  if(edge %in% tree$edge[,1])
    decs<-tree$edge[,2][which(tree$edge[,1]==edge)]
  else
    decs = NULL

  return(decs)
  #eof
}

# Identify extant tips
#
# @param taxa Edge label.
# @param tree Phylo object.
# @param tol Rounding error tolerance.
# @return Boolean (true/false).
# @examples
# t<-ape::rtree(6)
# is.extant(t$edge[,2][6],t)
is.extant<-function(taxa,tree,tol=NULL){

  if(is.null(tol))
    tol = min((min(tree$edge.length)/100),1e-8)

  ages = n.ages(tree)

  end = ages[taxa]

  if(end > (0 - tol) & end < (0 + tol))
    return(1)
  else return(0)

  # EOF
}
