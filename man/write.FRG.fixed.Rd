% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/generic.parser.R
\name{write.FRG.fixed}
\alias{write.FRG.fixed}
\title{Print out fossil range graph}
\usage{
write.FRG.fixed(tree, file = "", fossils = NULL)
}
\arguments{
\item{tree}{Phylo object.}

\item{file}{File name.}

\item{fossils}{Fossil object, essential if complete=FALSE.}
}
\value{
Prints out FRG table.
}
\description{
Parse fossil range graph input file for dppdiv from fossil occurrence data.
}
\examples{
t<-ape::rtree(6)
f<-FossilSim::sim.fossils.poisson(1,t)
write.FRG.fixed(t,fossils=f)
}
